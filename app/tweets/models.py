from app import db
from app import Base

class Tweet(Base)
    __tablename__ = 'tweet'
    vendor_id = db.Column(db.String(150))
    username = db.Column(db.String)
    lat = db.Column(db.Float)
    lng = db.Column(db.Float)
    prof_location = db.Column(db.String)
    text = db.Column(db.String)
