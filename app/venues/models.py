# Import the database object (db) from the main application module
# We will define this inside /app/__init__.py in the next sections.
from app import db
from random import randint
import boto
from app import Base

vendors = db.Table('vendors',
    db.Column('venue_id', db.Integer, db.ForeignKey('venue.id')),
    db.Column('vendor_id', db.Integer, db.ForeignKey('vendor.id'))
)

vendor_reviews = db.Table('vendor_reviews',
    db.Column('review_id', db.Integer, db.ForeignKey('review.id')),
    db.Column('vendor_id', db.Integer, db.ForeignKey('vendor.id'))
)

"""
vendor_tweets = db.Table('vendor_tweets',
    db.Column('tweet_id', db.Integer, db.ForeignKey('tweet.id')),
    db.Column('vendor_id', db.Integer, db.ForeignKey('vendor.id'))
)
"""


# Define a Venue model
class Venue(Base):

    __tablename__ = 'venue'
    id = db.Column(db.Integer, primary_key=True)
	
    venue_name = db.Column(db.String(128),  nullable=False)
    venue_id = db.Column(db.String(128))
    sports = db.Column(db.String(200))
    capacity = db.Column(db.Integer)
    latitude = db.Column(db.Float)
    longitude = db.Column(db.Float)
    venue_img = db.Column(db.String(250))
    vendors = db.relationship('Vendor', secondary=vendors,
        backref=db.backref('venues', lazy='dynamic'))

    @property
    def serialize(self):
        """
        For json serialization
        """
        return {
            'id': self.id,
            'venue_name': self.venue_name,
            'venue_id':self.venue_id,
            'sports': self.sports,
            'capacity':self.capacity,
            'venue_img':self.venue_img,
            'longitude':self.longitude,
            'latitude':self.latitude
        }
    def __repr__(self):
        return '<Venue %r>' % (self.venue_name)          

# Define a Vendor model
class Vendor(Base):
    __tablename__ = 'vendor'
    id = db.Column(db.Integer, primary_key=True)
    vendor_name = db.Column(db.String(128),  nullable=False)
    venue_id = db.Column(db.String(128))
    vendor_id = db.Column(db.String(128))
    section = db.Column(db.String(128))
    menu_id = db.Column(db.String(200))
    active = db.Column(db.Boolean)
    category = db.Column(db.String(100))
    vendor_img = db.Column(db.String(150))
    vendor_latitude = db.Column(db.Float(10))
    vendor_longitude = db.Column(db.Float(10))
    vendor_address = db.Column(db.String(150))
    vendor_address_2 = db.Column(db.String(150))
    vendor_state = db.Column(db.String(4))
    vendor_zip = db.Column(db.String(10))
    vendor_phone = db.Column(db.String(15))
    vendor_web = db.Column(db.String(20))
    vendor_average = db.Column(db.Float())
    reviews = db.relationship('Review', secondary=vendor_reviews,
        backref=db.backref('vendors', lazy='dynamic'))
    wait_times = db.relationship('Wait_time', backref='vendor',
                                lazy='dynamic')

    @property
    def serialize(self):
        """
        For json serialization
        """
        return {
            'id': self.id,
            'venue_id':self.venue_id,
            'vendor_id':self.vendor_id,
            'section':self.section,
            'vendor_img':self.vendor_img,
            'vendor_name': self.vendor_name,
            'vendor_latitude': self.vendor_latitude,
            'vendor_longitude': self.vendor_longitude,
            'vendor_address': self.vendor_address,
            'vendor_web': self.vendor_web,
            'category':self.category,
            'wait_time':randint(1,10) 
        }

    def calculate_density(self):
        """
        Calculate the crowd density
        """
	pass

    def __repr__(self):
        return '<Vendor %s>' % (self.vendor_name) 

class VendorDensity(Base):
    """
    """
    vendor_id = db.Column(db.String())
    start_date = db.Column(db.DateTime())
    end_date = db.Column(db.DateTime())
    total_orders = db.Column(db.Integer)
    density_score = db.Column(db.Integer, default=0)

class Menu(Base):
    __tablename__ = "menu"
    menu_name = db.Column(db.String(100))
    menu_id = db.Column(db.String(100))
    menu_description = db.Column(db.String(500))
    menu_category = db.Column(db.String(100))
    menu_items = db.relationship('Menu_item', backref='menu',
                                            lazy='dynamic')

    @property
    def serialize(self):
        return {
                "menu_id":self.menu_id,
                "menu_description":self.menu_description,
                "menu_items": [x.serialize for x in self.menu_items]
        }

class Menu_item(Base):
    __tablename__ = 'menu_item'
    item_name = db.Column(db.String(100))
    item_description = db.Column(db.String(200))
    item_id = db.Column(db.String(100))
    item_note = db.Column(db.String(100))
    item_price = db.Column(db.Float)
    menu_id = db.Column(db.Integer, db.ForeignKey('menu.id'))
    active = db.Column(db.Boolean)
    category = db.Column(db.String(140))

    @property
    def serialize(self):
        return {
            "item_name": self.item_name,
            "item_description": self.item_description,
            "item_id": self.item_id,
            "item_price": self.item_price,
            "category":self.category,
            "menu_id":self.menu_id
        }

class Wait_time(Base):
    __tablename__ = 'wait_time'
    venue_id = db.Column(db.String(150))
    vendor_id = db.Column(db.Integer, db.ForeignKey('vendor.id'))
    vendor_latitude = db.Column(db.Float(10))
    vendor_longitude = db.Column(db.Float(10))
    current_density = db.Column(db.Float(15))
    current_speed = db.Column(db.Float(10))
    current_approx_wait_time = db.Column(db.Float(10))

class Review(Base):
    __tablename__ = 'review'
    vendor_id = db.Column(db.String(150))
    vendor_name = db.Column(db.String(150))
    reviewer_name = db.Column(db.String(150))
    review_email = db.Column(db.String(150))
    review_note = db.Column(db.String(500))
    review_date = db.Column(db.String(100))
    review_stars = db.Column(db.Integer)
    @property
    def serialize(self):
        return {
            "review_id": self.id,
            "vendor_id":self.vendor_id,
            "vendor_name":self.vendor_name,
            "reviewer_name":self.reviewer_name,
            "reviewer_email":self.review_email,
            "review_note":self.review_note,
            "review_date":self.review_date,
            "review_stars":self.review_stars
        }

class Order(Base):
    __tablename__ = 'orders'

    order_id = db.Column(db.String(100))
    # user email
    user_id  = db.Column(db.String(200))
    vendor_id  = db.Column(db.String(200))
    order_items = db.relationship('Order_item', backref='orders',
                                            lazy='dynamic')
    vendor_name = db.Column(db.String(200))
    active = db.Column(db.Boolean())
    order_date = db.Column(db.DateTime, default=db.func.current_timestamp())

    @property
    def serialize(self):
        return {
            "order_id":self.id,
            "user_id":self.user_id,
            "vendor_id":self.vendor_id,
            "order_items":[x.serialize for x in self.order_items],
            "date_created":str(self.date_created)
        }
        

class Order_item(Base):
    __tablename__ = 'order_item'
    item_name = db.Column(db.String(100))
    item_description = db.Column(db.String(200))
    item_id = db.Column(db.String(100))
    item_note = db.Column(db.String(100))
    item_price = db.Column(db.Float)
    order_id = db.Column(db.Integer, db.ForeignKey('orders.id'))
    active = db.Column(db.Boolean)
    category = db.Column(db.String(140))
    quantity = db.Column(db.Integer)

    @property
    def serialize(self):
        return {
            "item_name": self.item_name,
            "item_id": self.item_id,
            "item_price": self.item_price,
            "category":self.category,
            "quantity":self.quantity
        }

