from models import *
from datetime import datetime, timedelta

present = datetime.now()
# get day of week
day_of_week = present.day
# get hour
hour = present.day
# get time within the last hour
last_hour = present - timedelta(hours=1)

# for every vendor
for vendor in vendors:
	vendor_id = vendor.vendor_id
	# get transactions within last hour
	orders = Orders.query.filter_by(vendor_id=vendor_id, date_created > last_hour).all()
	# get total
	total_orders = len(orders)
	vendor_prior = Vendor_prior.query.filter_by(
		vendor_id = vendor_id,
		day_of_week = day_of_week,
		hour = hour
	).last()
	try:
		old_average = vendor_prior.density_score
	except:
		old_average = 5

	new_average = (total_orders*10)/old_average

	# save total
	density = Vendor_density(
		vendor_id=vendor_id,
		start_date = last_hour,
		end_date = present
	)

	db.session.add(density)
	db.session.add(vendor_prior)
	db.session.commit()
db.session.close()
