# Import the database object (db) from the main application module
# We will define this inside /app/__init__.py in the next sections.
from app import db
#from vendor.models import Vendor
# Define a base model for other database tables to inherit
class Base(db.Model):

    __abstract__  = True

    id            = db.Column(db.Integer, primary_key=True)
    date_created  = db.Column(db.DateTime,  default=db.func.current_timestamp())
    date_modified = db.Column(db.DateTime,  default=db.func.current_timestamp(),
                                           onupdate=db.func.current_timestamp())


# Define a Venue model
class Calendar(Base):

    __tablename__ = 'calendar'

    id = db.Column(db.Integer, primary_key=True)
    event_id = db.Column(db.String(128))
    number = db.Column(db.Integer(10))
    date = db.Column(db.DateTime)
    event_type = db.Column(db.String(128))
    venue = db.Column(db.String(128))

    @property
    def serialize(self):
        """
        For json serialization
        """
        return {
            'id':self.id,
            'event_id': self.event_id,
            'number':self.number,
            'date':self.date,
            'event_type':self.event_type,
            'venue':venue
        }

