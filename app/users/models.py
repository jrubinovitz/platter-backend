# Import the database object (db) from the main application module
# We will define this inside /app/__init__.py in the next sections.
from app import db
#from vendor.models import Vendor
# Define a base model for other database tables to inherit
class Base(db.Model):

    __abstract__  = True

    id            = db.Column(db.Integer, primary_key=True)
    date_created  = db.Column(db.DateTime,  default=db.func.current_timestamp())
    date_modified = db.Column(db.DateTime,  default=db.func.current_timestamp(),
                                           onupdate=db.func.current_timestamp())


# Define a User model
class User(Base):

    __tablename__ = 'user'
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(128),  nullable=False)
    family_name = db.Column(db.String(128))
    gender = db.Column(db.Boolean())
    given_name = db.Column(db.String(100))
    url = db.Column(db.String(200))
    birthday = db.Column(db.String(100))
    picture = db.Column(db.String(200))
    verified_email = db.Column(db.String(100))
    aboutme= db.Column(db.String(500))

    @property
    def serialize(self):
        """
        For json serialization
        """
        return {
            'id':self.id,
            'email': self.email,
            'family_name':self.family_name,
            'gender':self.gender,
            'given_name':self.given_name,
            'hd':self.hd,
            'link':self.link,
            'locale':self.locale,
            'name':self.name,
            'verified_email':verified_email,
            'oauth_token':oauth_token
        }

class User_GPS(Base):
    __tablename__ = 'user_gps'
    user_email = db.Column(db.String(150))
    latitude = db.Column(db.Float())
    longitude = db.Column(db.Float())

