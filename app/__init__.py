# Import flask and template operators
from flask import Flask, render_template
from pymongo import MongoClient

# Import SQLAlchemy
from flask.ext.sqlalchemy import SQLAlchemy
import os

# Define the WSGI application object
app = Flask(__name__)

# Configurations
app.config.from_object('config')

# Define the database object which is imported
# by modules and controllers
db = SQLAlchemy(app)
mongo_url = os.getenv('MONGO_URL')
mongo_client = MongoClient('mongodb://'+mongo_url'+:27017/')

# Define a base model for other database tables to inherit
class Base(db.Model):

    __abstract__  = True

    id            = db.Column(db.Integer, primary_key=True)
    date_created  = db.Column(db.DateTime,  default=db.func.current_timestamp())
    date_modified = db.Column(db.DateTime,  default=db.func.current_timestamp(),
                                           onupdate=db.func.current_timestamp())



# Sample HTTP error handling
#@app.errorhandler(404)
#def not_found(error):
 #   return render_template('404.html'), 404

# Import a module / component using its blueprint handler variable (mod_auth)
#from app.venues.controllers import venues as venues_module
# Register blueprint(s)
#app.register_blueprint(venues_module)
# app.register_blueprint(xyz_module)
 # ..

# Build the database:
# This will create the database file using SQLAlchemy
db.create_all()
