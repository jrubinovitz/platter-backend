import os

# Statement for enabling the development environment
DEBUG = True

# Define the application directory
import os
#BASE_DIR = os.path.abspath(os.path.dirname(__file__))  


db_name = os.getenv('RDS_DB_NAME')
db_user = os.getenv('RDS_USERNAME')
db_password = os.getenv('RDS_PASSWORD')
db_host = os.getenv('RDS_HOSTNAME')
db_port = os.getenv('RDS_PORT')

# make db url 
db_url = "postgres://"+db_user+":"+db_password+"@"+db_host+":"+str(db_port)+"/"+ db_name
print db_url
# Define the database - we are working with
# SQLite for this example
SQLALCHEMY_DATABASE_URI = db_url 

# Application threads. A common general assumption is
# using 2 per available processor cores - to handle
# incoming requests using one and performing background
# operations using the other.
THREADS_PER_PAGE = 2

# Enable protection agains *Cross-site Request Forgery (CSRF)*
CSRF_ENABLED = False 

# Use a secure, unique and absolutely secret key for
# signing the data. 
CSRF_SESSION_KEY = "secret"

# Secret key for signing cookies
SECRET_KEY = os.getenv("SECRET_KEY") 

CELERY_BROKER_URL=os.getenv('CELERY_BROKER_URL'),
CELERY_RESULT_BACKEND=os.getenv('CELERY_BROKER_URL')
