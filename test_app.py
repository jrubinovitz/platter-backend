import unittest
from application import application as app 


class TestVendor(unittest.TestCase):
    def test_user_post(self):
        # Use Flask's test client for our test.
        self.test_app = app.test_client()
 
        # Make a test request to add a user, supplying fake data
        response = self.test_app.post('/vendors', data={'vendor_id':
            'platterrio2016020BeerNWine204'})
 
        # Assert response is 200 OK.
        self.assertEquals(response.status, "200 OK")

class TestUser(unittest.TestCase):
    def test_user_post(self):
        # Use Flask's test client for our test.
        self.test_app = app.test_client()
 
        # Make a test request to add a user, supplying fake data
        response = self.test_app.post('/users/', data={'family_name':
            'Bob'})
 
        # Assert response is 200 OK.
        self.assertEquals(response.status, "200 OK")

class TestMenu(unittest.TestCase):
    def test_menu_post(self):
        # Use Flask's test client for our test.
        self.test_app = app.test_client()
 
        # Make a test request to get a menu, supplying fake data
        response = self.test_app.post('/menus/', data={'vendor_id':
            'platterrio2016011RioHotDogs351'})
        print response.data 
        # Assert response is 200 OK.
        self.assertEquals(response.status, "200 OK")

class TestReview(unittest.TestCase):
    def test_review_post(self):
        # Use Flask's test client for our test.
        self.test_app = app.test_client()
 
        # Make a test request to get a menu, supplying fake data
        response = self.test_app.post('/reviews/', data={'vendor_id':
            'platterrio2016011RioHotDogs351'})
        #print response.data 
        # Assert response is 200 OK.
        self.assertEquals(response.status, "200 OK")

class TestGPS(unittest.TestCase):
    def test_gps_post(self):
        # Use Flask's test client for our test.
        self.test_app = app.test_client()
 
        # Make a test request to get a menu, supplying fake data
        response = self.test_app.post('/gps/', data={'email':
            'j@j.com','latitude':"-13","longitude":"12"})
        print response.data 
        # Assert response is 200 OK.
        self.assertEquals(response.status, "200 OK")

class TestOrdering(unittest.TestCase):
    def test_order_post(self):
        # Use Flask's test client for our test.
        self.test_app = app.test_client()
 
        # Make a test request to get a menu, supplying fake data
        response = self.test_app.post('/orders/', data={'item_ids':
            "['platterrio2016011RioHotDogs351025','platterrio2016011RioHotDogs351028','platterrio2016011RioHotDogs351053']",'item_quantities':"[1,2,3]",'email':'j@j.com'})
        print response.data 
        # Assert response is 200 OK.
        self.assertEquals(response.status, "200 OK")

    def test_order_get(self):

        # Use Flask's test client for our test.
        self.test_app = app.test_client()
 
        # Make a test request to get a menu, supplying fake data
        response = self.test_app.post('/orders/user/', data={'email':'j@j.com'})
        print response.data 
        # Assert response is 200 OK.
        self.assertEquals(response.status, "200 OK")

