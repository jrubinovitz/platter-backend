/* Analyze overnight results */
a = load $INPUT;
b = foreach a generate flatten(TOKENIZE((chararray)$0)) as word;
c = group b by word;
d = foreach c generate COUNT(b), group;
e = ORDER d by $1;
store e into $OUTPUT;
