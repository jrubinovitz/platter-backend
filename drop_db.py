from app import app as application 

from app import db
# Import module models (i.e. User)
from app.venues.models import Venue, Vendor, Menu, Menu_item
from app.users.models import User


db.drop_all()
