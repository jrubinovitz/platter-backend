from app import db

from app.venues.models import Venue, Vendor, Menu, Menu_item, Review, Order, Order_item

from app.users.models import User, User_GPS

import csv

def load_venues():
    with open('./dumps/Venue.tsv', 'rb') as csvfile:
        reader = csv.reader(csvfile, delimiter='\t', quotechar='|')
        firstRow = True
        for row in reader:
            if not firstRow:
                venue_id = row[0]
                venue_name = row[1]
                sports = row[2]
                capacity = int(row[3])
                latitude = float(row[4][:-1])
                longitude = float(row[5])
                venue_img = row[6]
                new_venue = Venue(venue_id = venue_id, venue_name=venue_name, sports=sports, capacity=capacity, latitude=latitude, longitude=longitude, venue_img=venue_img)
                db.session.add(new_venue)
                db.session.commit()
            else:
                firstRow = False

            db.session.close()

def load_vendors():
    with open('./dumps/Vendor.tsv', 'rb') as csvfile:
        reader = csv.reader(csvfile, delimiter='\t', quotechar='|')
        firstRow = True
        last_venue_id = None
        last_venue = None
        for row in reader:
            if not firstRow:
                venue_id = row[0]
                vendor_id = row[1]
                vendor_name = row[2]
                section = row[3]
                category = row[4]
                vendor_img = row[5]
                vendor_latitude = float(row[6])
                vendor_longitude = float(row[7])
                vendor_address = row[8]
                vendor_address_2  = row[9]
                vendor_city = row[10]
                vendor_state = row[11]
                vendor_zip = row[12]
                vendor_phone = row[13]
                vendor_web = row[14]
                new_vendor = Vendor(venue_id=venue_id, vendor_id=vendor_id, vendor_name=vendor_name,section=section, category=category, vendor_img=vendor_img, vendor_latitude=vendor_latitude, vendor_longitude=vendor_longitude, vendor_address=vendor_address, vendor_address_2=vendor_address_2, vendor_state=vendor_state, vendor_zip=vendor_zip, vendor_phone=vendor_phone, vendor_web=vendor_web)                   
                if venue_id != last_venue_id:
                    last_venue = Venue.query.filter_by(venue_id=venue_id).first()          
                    last_venue_id = venue_id
                if last_venue:
                    db.session.add(new_vendor)
                    db.session.commit()
                    last_venue.vendors.append(new_vendor)
                    db.session.add(last_venue)
                    db.session.commit()
            else:
                firstRow = False

    db.session.close()



def load_menus():
    with open('./dumps/Menu_2.tsv', 'rb') as csvfile:
        reader = csv.reader(csvfile, delimiter='\t', quotechar='|')
        firstRow = True
        last_menu_id = None
        last_menu = None
        for row in reader:
            if not firstRow:
                menu_id,menu_name,menu_description,menu_category_name,item_id,item_name,item_description,item_note,price,active = row
                try:
                    item_price = float(price)
                except:
                    item_price = 0
                if active == "y":
                    active = True
                else:
                    active = False
                try:
                    menu_item = Menu_item(item_name=item_name, category=menu_category_name,item_description=item_description, item_note=item_note, item_price=item_price, active=active, item_id=item_id)
                    db.session.add(menu_item)
                    
                    if last_menu_id != menu_id:
                        last_menu = Menu(menu_id=menu_id, menu_description=menu_description, menu_name=menu_name)
                        last_menu_id = menu_id
                    last_menu.menu_items.append(menu_item)
                    db.session.add(last_menu)
                    db.session.commit()
                except:
                    pass
            else:
                firstRow = False
    db.session.close()

def load_reviews():
    with open('./dumps/Reviews.tsv', 'rb') as csvfile:
        reader = csv.reader(csvfile, delimiter='\t', quotechar='|')
        firstRow = True
        for row in reader:
            if not firstRow:
                vendor_id,vendor_name,reviewer_name,reviewer_email,review_id,review_note,review_date,review_stars = row
                review_stars = int(review_stars)
                review = Review(vendor_id=vendor_id, vendor_name=vendor_name, reviewer_name=reviewer_name, review_email=reviewer_email, review_note=review_note, review_date=review_date, review_stars=review_stars)
                db.session.add(review)
                db.session.commit()
            else:
                firstRow = False
    db.session.close()



load_venues()
load_vendors()
load_menus()
load_reviews()
