import boto
import datetime
from datetime import timedelta
import gc
import numpy as np
from pymongo import MongoClient
import string
from app.venues.models import Venue, Vendor, VendorDensity
import sys
import os
from  app import mongo_client as client
from sqlalchemy.sql import func

def perdelta(start, end, delta):
    curr = start
    while curr < end:
	yield curr, min(curr + delta, end)
	curr += delta

class CrowdDensity:

    def __init__(self, population, vendors, start_year, start_month, start_day, end_year, end_month, end_day, aws_access_key_id, aws_secret_access_key, data_path):
	# estimated population at event
	self.population = population 
	# number of vendors at event
	self.vendors = vendors

	self.start = start 
	self.end = end
	step = step

	# AWS credentials
	self.aws_access_key_id = aws_access_key_id

	self.aws_secret_access_key = aws_access_key_id
	
	# S3 data path
	self.data_path = data_path


    # get mongo tweets
    def get_mongo_tweets(dates):
	"""
	Get tweets from mongoDB and store into file to push up to S3 so pig can process
	"""
	for date in dates:
	    day = 1
	    outfile_name = date.strftime("%Y-%m-%d") + "_tweets.txt" 
	    outfile = open(outfile_name, "w")
	    db = client['test']
	    collection_names = db.collection_names()
	    exclude = set(string.punctuation)
	    
	    for collection_name in collection_names:
		all_tweets =  db[collection_name].find()

		for tweet in all_tweets:
		    text = tweet['twitter']['text'].lower()
		    remove_hash = [word.replace("#", "") for word in text]
		    remove_hash = ("").join(remove_hash)
		    new_text = ("").join(ch for ch in remove_hash if ch not in exclude).encode('utf-8').strip()
		    outfile.write(new_text + "\n")
		
		# move file to AWS with boto
		full_key_name = os.path.join(data_path, outfile_name)
		k = bucket.new_key(full_key_name)
		k.set_contents_from_filename(outfile)
		day = day + 1

    def simulate_venues():
	pass

    def simulate_vendors():
	pass

    def simulate_transactions():
       number_of_venues = len(Venues.query.all()) 

    # get vendors from tweets
    def analyze_twitter_data():
	    #sample_vendors = open("twitter_vendors.txt", "r").read().split("\n")
	    #for vendor_name in sample_vendors:
		    #get tweets with vendor name
	    #	tweets = Tweet.query.filter_by(vendor_id=vendor_name).all()
	pass
    def analyze_current_transaction_data(time=None):
	"""
	To be cron-jobbed hourly.

	"""
	if not time:
	    time = datetime.datetime.now()

	# get current day of week
	weekday = time.weekday()
	# get current hour of day
	hour = time.hour
	# an hour from time
	hour_ago = time - time.timedelta(hours=1)
	# get transactions of the last hour of this day
	new_orders = Order.query.filter(Order.order_date > hour_ago, Order.order_date <= time)
	# get prior transaction averages for hour on this weekday 
	prior_orders = Order.query.filter(func.strftime('%h', Order.order_date) == str(hour), func.strftime('%w', Order.order_date) == str(weekday)).all()

	# number of all prior orders
	num_all_orders = db.session.query(Orders).count()
	# get average of all priors
	avg_all = VendorDensity.query.with_entities(func.avg(VendorDensity.total_orders).label('average')).filter()
	# get average of last hour
	avg_hour = VendorDensity.query.with_entities(func.avg(VendorDensity.total_orders).label('average')).filter(func.strftime('%h', Order.order_date) == str(hour), func.strftime('%w', Order.order_date) == str(weekday))

    def to_csv():
	"""
	Move new transaction data to CSV so it can be processed by mapreduce overnight.
	"""
	#order_csv = open()
	#orders = Order.query.all()
	#for order in orders:
	#	gc.collect(orders)
	pass
#get_mongo_tweets()
