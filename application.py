from app import app as application 

# Import the database object from the main app module
from app import db

# Import module models (i.e. User)
from app.venues.models import Venue, Vendor, Menu, Menu_item, Review, Order, Order_item

from app.users.models import User, User_GPS

from flask import request, jsonify

import csv

db.create_all()

# index page
@application.route('/')
def hello_world():
        return "Hello world!"

# venues data 
@application.route('/venues', methods=['GET', 'POST'])
def get_venues(): 
        print 'get venues'
        args = request.args
        venue_id = args.get('venue_id')
        # if searching fo specific venue
        if venue_id:
            try:
                # get all venues with id
                venues = Venue.query.filter_by(venue_id=venue_id).all()
                venues = venue.query.all()
                venues_list = [venue.serialize for venue in venues]
                return jsonify(objects=venues_list)

            # if venue_id doesnt exist
            except:
                return jsonify(objects=[], error="No such venue_id")


        venues = Venue.query.all()
        venues_list = [venue.serialize for venue in venues]
        return jsonify(objects=venues_list)


# get vendor by id
@application.route('/vendors/<vendor_id>', methods=["GET","POST"])
def vendors_get(vendor_id):
 
    vendors = Vendor.query.filter_by(vendor_id=vendor_id).first()
    return jsonify(objects=vendors.serialize)
    

# get all vendors
@application.route('/vendors', methods=["GET","POST"])
def vendors_api():
    args = request.form
 
    venue_id = args.get('venue_id')
    vendor_id = args.get('vendor_id')
    if venue_id:
        vendors = Vendor.query.filter_by(venue_id=venue_id).first()
        return jsonify(objects=vendors.serialize)
    
    if vendor_id:
        vendors = Vendor.query.filter_by(vendor_id=vendor_id).first()
        return jsonify(vendors.serialize)
    vendors = Vendor.query.all()
    vendors_list = [x.serialize for x in vendors]
    return jsonify(objects=vendors_list)

# get menus
@application.route('/menus', methods=["GET","POST"])
def menu_api():
    args = request.form
    item_id = args.get('item_id')
    if item_id:
        return Menu_item.query.filter_by(item_id=item_id).first().serialize
    menus = Menu.query.all()
    menu_list = [x.serialize for x in menus]
    return jsonify(objects=menu_list)

# get reviews
@application.route('/reviews/', methods=["GET","POST"])
def reviews_api():
    args = request.form
    if not args:
        args = request.json
    if not args:
        return jsonify(objects=[x.serialize for x in Review.query.all()])
    vendor_id = args.get('vendor_id')
    if not vendor_id:
        return jsonify(status=500, error="No vendor_id given for menu")

    menu = Review.query.filter_by(vendor_id=vendor_id).first().serialize

    return jsonify(objects=menu, status=200)


@application.route('/menus/', methods=["GET","POST"])
def menus_api():
    args = request.form
    if not args:
        args = request.json
    if not args:
        return jsonify(status=500, error="No json args given")
    vendor_id = args.get('vendor_id')
    if not vendor_id:
        return jsonify(status=500, error="No vendor_id given for menu")

    menu = Menu.query.filter_by(menu_id=vendor_id).first().serialize

    return jsonify(objects=menu, status=200)


@application.route('/orders/user/',methods=["GET","POST"])
def orders_user():
    args = request.form
    if not args:
        args = request.json
    if not args: 
        return jsonify(status=500, error="No json args given")

    email = args.get('email')
    if  email:
        orders = Order.query.filter_by(user_id=email).all()
        orders_ser = [x.serialize for x in orders]
        return jsonify(objects=orders_ser)


@application.route('/orders/', methods=["GET","POST"])
def orders():
    args = request.form
    if not args:
        args = request.json
    if not args: 
        return jsonify(status=500, error="No json args given")
    item_ids = args.get('item_ids')
    item_quantities = args.get('item_quantities')
    order_id = args.get('order_id')
    email = args.get('email')
   # if sending order:
    if item_ids:
        items = []
        item_ids = eval(item_ids)
        item_quantities = eval(item_quantities)
        if len(item_ids) != len(item_quantities):
            return jsonify(status=500, error="Unequal number of items and quantities")
    
        for i in range(len(item_ids)):
            item_id = str(item_ids[i])
            new_item = Menu_item.query.filter_by(item_id=item_id).first()
            new_order_item = Order_item(
                item_name=new_item.item_name, 
                item_id=item_id, 
                item_price=new_item.item_price, 
                category=new_item.category,
                quantity=item_quantities[i]
            )
            db.session.add(new_order_item)
            db.session.commit()
            items.append(new_order_item)
            menu_id = new_item.menu.menu_id
            vendor = Vendor.query.filter_by(vendor_id=menu_id).first()
            vendor_name = vendor.vendor_name
            vendor_id = vendor.vendor_id

        new_order = Order(user_id=email, order_items=items, vendor_id=vendor_id, vendor_name=vendor_name, active=True)
        db.session.add(new_order)
        db.session.commit()
        order_id = new_order.id
        db.session.close()
        return jsonify(status=200, order_id=order_id, message="Order successfully added!")
    # if retrieving order
    elif order_id:
        return Order.query.filter_by(id=order_id).first().serialize

    else:
        return jsonify(status=500,message="I do not understand the data your are giving me.")

@application.route('/gps/', methods=["GET","POST"])
def gps_api():
    args = request.form
    if not args:
        args = request.json
    if not args:
        return jsonify(status=500, error="No json args given")
    latitude = float(args.get('latitude'))
    longitude = float(args.get('longitude'))
    user_email = args.get('user_email')
    if not user_email:
        user_email = args.get('email')

    gps = User_GPS(latitude=latitude, longitude=longitude, user_email=user_email)
    db.session.add(gps)
    db.session.commit()
    db.session.close()
    return jsonify(status=200, message="Successfully added location!")

@application.route('/users/', methods=["GET","POST"])
def user_api():
    args = request.form
    if not args:
        args = request.json
    if not args:
        return jsonify(status=500, error="No json args given")
    if request.method == "POST":
        try:
            email = args.get('email')
            family_name = args.get('family_name')
            gender = args.get('gender')
            given_name = args.get('given_name')
            birthday = args.get('birthday')
            url = args.get('url')
            picture = args.get('picture')
            aboutme = args.get('aboutme')
            verified_email = args.get('verified_email')
            if gender == "1":
                gender = True
            else:
                gender = False
            user = User(email=email,family_name=family_name,gender=gender,given_name=given_name,url=url,picture=picture,aboutme=aboutme, verified_email=verified_email)
            db.session.add(user)
            db.session.commit()
            db.session.close()
            return jsonify(status=200,message="User data stored successfully!")
        except:
            return jsonify(status=500, error="Error storing user")
    if request.method == "GET":
        user_id = args.get("email")
        return jsonify(objects=User.query.filter_by(email=email).all())

@application.route("/calendar", methods=["GET","POST"])
def calendar_api():
    args = request.args()
    if request.method == "GET":
        events = Calendar.query.all()
        return jsonify(objects=events)

if __name__ == '__main__':
    application.run(host='0.0.0.0', debug=True)
