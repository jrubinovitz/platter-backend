# -*- coding:utf-8 -*-

from flask.ext.script import Command, Option, prompt_bool
import os
import config
import datetime
from datetime import timedelta
from sqlalchemy import func
from simulate import *
from load_data import *

population = 30000000
orders = population * 3
# number of vendors at event
vendors = 100
mu, sigma = 5, 0.1
# get normal distribution for number of orders 
sample = np.random.normal(mu, sigma, orders)

start = datetime.datetime(2012, 7, 25)
end = datetime.datetime(2012, 8, 12)
step = datetime.timedelta(days=1)

class CreateDB(Command):
    """
    Creates sqlalchemy database
    """

    def run(self):
        from database import create_all

        create_all()


class DropDB(Command):
    """
    Drops sqlalchemy database
    """

    def run(self):
        from database import drop_all

        drop_all()

class SimulateData(Command):
    """
    Simulate venues, vendors, and orders. Vendors and venues come from spreadsheets, orders are created at random on a normal distribution.
    """
    def run(self):
	load_venues()
	load_vendors()
	load_menus()
	load_reviews()
	self.simulate_orders()

    def simulate_orders(self):
	venues = Venues.query.all()
	for venue in venues:
	    vendors = venue.vendors
	    for i in range(len(vendors)):
		vendor = vendors[i]
		number_of_orders = population * sample 
		menu_id = vendor.menu_id
		menu = Menu.query.filter_by(menu_id=menu_idd).first()
		menu_items = menu.menu_items
		number_menu_items = len(menu_items)
		menu_item_sample = np.random.normal(mu, sigma, number_menu_items)
		for j in range(len(menu_item)):
		    pass	
 
class DumpTweets(Command):
    """
    Dump tweets for the day from MongoDB into text files
    """

class AnalyzeTweets(Command):
    
    # allow user to enter 
    # python manage.py scale --all=True
    # to mine only today's tweets
    option_list = (
        Option('--all', '-n', dest='all'),
    ) 
    def run(self, all=False):
        """
	"""
	pass

class AnalyzeTransactions(Command):
    """
    """
    option_list = (
	    Option('--scope', '-n', dest="scope"),
	    Option('--date', '-n', dest="date"),
	    Option('--event', '-n', dest="event"),
    )

    def run(self, date, event, scope="hourly"):
	pass	

class Test(Command):
    """
    Run tests
    """

    start_discovery_dir = "tests"

    def get_options(self):
        return [
            Option('--start_discover', '-s', dest='start_discovery',
                   help='Pattern to search for features',
                   default=self.start_discovery_dir),
        ]

    def run(self, start_discovery):
        import unittest

        if os.path.exists(start_discovery):
            argv = [config.project_name, "discover"]
            argv += ["-s", start_discovery]
            print argv
            unittest.main(argv=argv)
        else:
            print("Directory '%s' was not found in project root." % start_discovery)
