Platter Backend
===
This is the server side code for the Platter app.

Environment Variables
---
Set the following environment variables on your machine

* RDS_HOST - host of mysql database
* RDS_PORT - port of mysql database
* RDS_USERNAME - name of database user
* RSD_DB_NAME - name of database
* SECRET_KEY - string for encryption
* AWS_SECRET_KEY_ID - AWS key id that has permissions for platter-data bucket
*  AWS_SECRET_ACCESS_KEY - AWS secret for platter-data bucket

Installation on an Ubuntu Machine
---
```
sudo apt-get install python-dev
sudo apt-get install python-setuptools
sudo easy_install pip
sudo apt-get install python-virtualenv
virtualenv venv
. venv/bin/activate
pip install -r requirements.txt
python load_data.py
```

Running the app
---
```
python application.py
```

Running the MongoDB Amazon Server
---
See [guide](https://docs.google.com/document/d/1q4SvRwQcO4NySxjxp-sjUOOhxXVR58Hv18wjlM2hXoE/edit?usp=drive_web)
```
mongod --fork --logpath mongo.log
```

Cronjob Python Calculations
---
- Get new prior for day ( daily 5:00 AM)
- Average over last hour (hourly)
- Make CSV of new averages for pig (daily 2:00 AM)

Pig Calculations
---
- Weekday average
- Time of day average
- Vendor average
